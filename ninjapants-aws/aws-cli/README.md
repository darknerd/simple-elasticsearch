# **AWS CLI**

This creates systems on AWS using AWS CLI tools.  You must have those tools installed and configure proper account credentials to create EC2 systems.

```bash
# create SG group
./ec2-sg.sh
# export SG_group we created
export SECURITY_GROUPS=$(aws ec2 describe-security-groups \
                     --filter "Name=tag-value,Values=dk-security" \
                     --query 'SecurityGroups[].GroupId' \
                     --output text)
# create tools server
./ec2-instance-tools.sh
# create elastic search servers
./ec2-instance-elasticsearch.sh
```
