# These are place holders for existing AWS Infrastructure

#####################################################################
# Imported VPC
#####################################################################
resource "aws_vpc" "default-vpc" {
  cidr_block           = "172.31.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  instance_tenancy     = "default"
}

#####################################################################
# Imported Subnets
#####################################################################
resource "aws_subnet" "subnet-us-east-2a" {
  vpc_id                  = "${aws_vpc.default-vpc.id}"
  cidr_block              = "172.31.0.0/20"
  availability_zone       = "us-east-2a"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "subnet-us-east-2b" {
  vpc_id                  = "${aws_vpc.default-vpc.id}"
  cidr_block              = "172.31.16.0/20"
  availability_zone       = "us-east-2b"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "subnet-us-east-2c" {
  vpc_id                  = "${aws_vpc.default-vpc.id}"
  cidr_block              = "172.31.32.0/20"
  availability_zone       = "us-east-2c"
  map_public_ip_on_launch = true
}
