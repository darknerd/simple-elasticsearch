# **Terraform**  

## **Getting Started**

1. Download and Install Terraform: https://www.terraform.io/downloads.html
1. Initialize Terraform Environment `terraform init`
1. Import infrastructure state for vpc and subnets

**WARNING**: This is dangerous.  It is vital you import your default vpc and subsequent subnets (one per availability zone).  If you are using for a region that is not "us-east-2", then you need to add entries to for VPCs and subnets that make sense to your region.  Alternatively, you can build custom VPCs and subnets from scratch.

```bash
# Initialize Environment
export TF_VAR_region="us-east-2"
terraform init

# Import Resources
VPC_ID="$(aws ec2 describe-vpcs \
           --filters 'Name=isDefault,Values=true' \
           --query 'Vpcs[].VpcId' \
           --output text)"

terraform import aws_vpc.default-vpc $VPC_ID

SUBNET_TABLE="$(aws ec2 describe-subnets \
            --filter "Name=vpc-id,Values=${VPC_ID}" \
            --query 'Subnets[].[AvailabilityZone,SubnetId]' \
            --output text)"

while read ZONE SUBNET_ID; do
  terraform import aws_subnet.subnet-${ZONE} ${SUBNET_ID}
done <<< "${SUBNET_TABLE}"
```
## **Create Some Instances**

This creates security group to allow you to ssh into the box from a public IP and allow the instances to communicate with each other.  Make sure that SSH keys were generated and installed previously.

If you created systems with similar name (tag of name), you may want to delete them, as this will cause confusion with bootstrapping or provisioning the systems.

```bash
terraform plan  # see what you will create
terraform apply # create the systems and sg groups
```

## **Tips**

### **Gathering Info About Infrastructure**

You can use a tool like [terraforming](http://terraforming.dtan4.net/) (`gem install terraforming`) to investigate current cloud resources.

```bash
terraforming sg
terraforming vpc
terraforming sn # vpc subnets
```
