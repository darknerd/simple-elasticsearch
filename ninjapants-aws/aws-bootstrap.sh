#!/usr/bin/env bash

# Find Config Path from Root Project
[[ -d config ]] && CONFIG_PATH=./config
[[ -d ../config ]] && CONFIG_PATH=../config
[[ -z "${CONFIG_PATH}" ]] && { echo "Error: Cannot Bootstrap Script" 1>&2; exit 1; }

# RUn Script for AWS mode
cd ${CONFIG_PATH} && CHEF_ENV=dk_aws ./bootstrap.sh
