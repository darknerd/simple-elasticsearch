# **Google Cloud**

## **Getting Started**

To get started with Google Cloud, you will need to do the following:

1. Create a new [billing account](https://cloud.google.com/billing/docs/how-to/manage-billing-account):
   * create google account if you do have one already
   * enable billing for that account (free trial)
1. Create a Google Project (or use default)
1. Enable [billing for that project](https://cloud.google.com/billing/docs/how-to/modify-project):
1. Install [Google Cloud SDK](https://cloud.google.com/sdk/)
1. Authorize gcloud tools
   * https://cloud.google.com/compute/docs/gcloud-compute/

I wrote some blogs on juggling multiple google accounts or getting started with Chef and GCP:

* [Juggling Accounts with GCloud SDK](https://medium.com/@Joachim8675309/juggling-accounts-with-gcloud-sdk-f12f282a439c)
* [Starting with Chef on GCP](https://medium.com/@Joachim8675309/starting-with-chef-on-gcp-b1a33a721c17)


### **Ubuntu Install Script**

You can install Ubuntu using shell script:

```bash
./gcloud_ubuntu_install.sh
```

Or you can use the supplied Chef recipe (assumes ChefDK is installed):

```bash
sudo chef exec chef-apply gcloud_ubuntu_recipe.rb
```

## **Generate SSH Key Pair**

Generate an SSH key pair that will use deployments and configuration of GCE instances.  Install this key for the `ubuntu` user into your project's metadata.  A sample script is provided to demonstrate how to do this:

```bash
# if current project is correct, can set GCP_PROJECT to that
export GCP_PROJECT=$(gcloud config list 2>&1 | tr -d ' ' | awk -F= '/project/ { print $2 }')
./gce-genkey.sh
```

## **Provision Systems**

Provision a system with one of these tools:

* [gcloud command line](gcloud/README.md)
* [terraform](terraform/README.md)

## **Gathering GCE Instance Info**

```bash
# list gce instances for current project
gcloud compute instances list
# list metadata (yaml) for given gce instance
gcloud compute instances describe ${GCP_INSTANCE_NAME}
```

## **Logging into GCE Instance**

After this, you can log into these systems with:

```bash
ssh -i .sekrets/gce.key ubuntu@$EXT_IPADDRESS
```

## **BootStrap Systems with Chef**

The example script included will detect the systems created on googlecloud, and then provision them.  This will configure a unicast discovery list that ElasticSearch will use to find other members in its cluster.

```bash
./gce-bootstrap.sh
```

Note, if you used these public IP address to provision a system and have auth errors, you may need to purge the entry in known_hosts:

```bash
ssh-keygen -f ~/.ssh/known_hosts -R $EXT_IPADDRESS
```
