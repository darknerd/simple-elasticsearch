# **Chef GCompute Cookbook**

*Infrastructure as Code* using the Chef `google-gcompute` cookbook.

The purpose of this is to have minimalist way to create GCE instances using Chef. The credentials should leverage off current configuration in `gcloud` (minimalist approach).

## **Status**

Currently this solution does not work for default credentials with `gcloud`:

* https://github.com/GoogleCloudPlatform/chef-google-auth/issues/2

## **Observations**

Chef does not support using `google-gcompute` cookbook with either `chef-apply recipe.rb` or `chef-client -z recipe.rb`.  This is because dependencies to cookbooks are only supported when using a cookbook.  Thus, unfortunately, the user is burden with creating a full chef repo with cookbooks just to run a small infrastructure script.

## **Instructions**

```bash
chef-client \
 --local-mode \
 --runlist 'recipe[dk_gcloud]'
```

or

```bash
chef-client \
 --config .chef/knife.rb \
 --runlist 'recipe[dk_gcloud]'
```
