name 'dk_gcloud'
maintainer 'Joaquin Menchaca'
maintainer_email 'suavecali@yahoo.com'
license 'MIT'
description 'creates instances on gcloud'
long_description 'creates instances on gcloud'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

depends 'google-gcompute', '~> 0.1.1'
