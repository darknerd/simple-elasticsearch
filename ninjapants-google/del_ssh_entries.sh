#!/usr/bin/env bash

# Fetch List of Systems in our Project
GKE_SYSTEMS=$(gcloud compute instances list | awk '!/NAME/ { printf "%s %s\n", $1, $5 }')

# Purge ALL entries for servers w/ "tools" or "es" in name
while read SYSTEM EXT_IPADDR; do
  if [[ $SYSTEM =~ (tools|es) ]]; then
    ssh-keygen -f ${HOME}/.ssh/known_hosts -R $EXT_IPADDR
  fi
done <<< "${GKE_SYSTEMS}"
