apt_repository 'google-cloud-sdk' do
  key 'https://packages.cloud.google.com/apt/doc/apt-key.gpg'
  uri 'http://packages.cloud.google.com/apt'
  distribution "cloud-sdk-#{node['lsb']['codename']}"
  components ['main']
end
