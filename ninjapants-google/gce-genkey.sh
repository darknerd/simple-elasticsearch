#!/usr/bin/env bash
[[ -z "${GCP_PROJECT}" ]] && { echo 'Error: GCP_PROJECT not set. Exiting' 1>&2; exit 1; }

KEYPATH=".sekrets"
GCE_KEYS="${KEYPATH}/${GCP_PROJECT}.ssh-keys"

# Generate Key Pair
ssh-keygen -t rsa -b 4096 -f "${KEYPATH}/gce.key" -C ubuntu -q -N ""

# Create Google Style ssh-key dsv (colon sep file)
printf '%s:%s\n' 'ubuntu' "$(cat ${KEYPATH}/gce.key.pub)" >> ${GCE_KEYS}

# Install Keys into Metadata
gcloud compute project-info add-metadata \
  --metadata-from-file ssh-keys=${GCE_KEYS}

# Backup Key to ~/.ssh/
cp ${KEYPATH}/gce.key ${HOME}/.ssh
