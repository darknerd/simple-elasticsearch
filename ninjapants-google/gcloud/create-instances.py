#!/usr/bin/env python2

import os
import subprocess
import re

try:
    import json
except:
    import simplejson as json

gcp_project = os.environ['GCP_PROJECT'] or Exception("GCP_PROJECT not set!!").throw()
gcp_service_acct = os.environ['GCP_SERVICE_ACCT']  or Exception("GCP_SERVICE_ACCT not set!!").throw()
config_file = os.environ.get('CONFIG_DATA') or 'gce.json'

configs = json.loads(open(config_file).read())

for system, config in configs.iteritems():
    # reformat lists into comma-separated string
    scopes = ','.join(config['scopes'])
    tags = ','.join(config['tags'])

    # set tag options to empty string if there are no tags
    tags_options = '' if tags == '' else "--tags={}".format(tags)

    # format dictionary used for string formatting
    gcloud_command_format = {
        'system': system,
        'project': gcp_project,
        'zone': config['zone'],
        'machine-type': config['machine-type'],
        'service-account': gcp_service_acct,
        'scopes': scopes,
        'tags_options': tags_options,
        'image': config['image'],
        'image-project': config['image-project'],
        'boot-disk-type': config['boot-disk-type'],
        'boot-disk-size': config['boot-disk-size'],
        'boot-disk-device-name': system
    }

    # create formatted gcloud command
    gcloud_command = r"""gcloud compute --project={project} instances create {system} \
  --zone={zone} \
  --machine-type={machine-type} \
  --subnet=default \
  --maintenance-policy=MIGRATE \
  --service-account={service-account} \
  --scopes={scopes} \
  {tags_options} \
  --image={image} \
  --image-project={image-project} \
  --boot-disk-size={boot-disk-size} \
  --boot-disk-type={boot-disk-type} \
  --boot-disk-device-name={system}
    """.format(**gcloud_command_format)

    process = subprocess.Popen(gcloud_command, stdout=subprocess.PIPE, shell=True)

    for metakey, metavalue in config['metadata'].iteritems():
        # build named format hash for processing commands
        gcloud_command_format = {
          'system': system,
          'zone': config['zone'],
          'metakey': metakey,
          'metavalue': metavalue
        }

        # foormat status command
        status_cmd = r"""gcloud compute instances list \
          --filter="name={system} AND zone ~ {zone}" \
          --format='table(name,status)'
        """.format(**gcloud_command_format)

        # run status command to fetch status
        process = subprocess.Popen(status_cmd, stdout=subprocess.PIPE, shell=True)
        (output, err) = process.communicate()
        if not output: break

        # extract status from output
        status = list(filter(lambda line: system in line, output.split("\n")))[0].split()[-1]

        if re.match('RUNNING', status):
            add_metadata_cmd = r"""gcloud compute instances add-metadata {system} \
              --zone={zone} \
              --metadata "{metakey}={metavalue}"\n""".format(**gcloud_command_format)
            subprocess.Popen(add_metadata_cmd, stdout=subprocess.PIPE, shell=True)
