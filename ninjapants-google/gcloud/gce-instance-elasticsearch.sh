#!/usr/bin/env bash

### Check Required Env Variables
[[ -z "${GCP_SERVICE_ACCT}" ]] && { echo 'Error: GCP_SERVICE_ACCT not set. Exiting' 1>&2; exit 1; }
[[ -z "${GCP_PROJECT}" ]] && { echo 'Error: GCP_PROJECT not set. Exiting' 1>&2; exit 1; }

### Create Scope String
SCOPES=(devstorage.read_only
       logging.write
       monitoring.write
       pubsub
       servicecontrol
       service.management.readonly
       trace.append
       userinfo.email
       compute)
PREFIX='https://www.googleapis.com/auth'
SCOPE_URLS=$(IFS=, ; echo "${SCOPES[*]/#/$PREFIX/}")

declare -A ZONES
ZONES+=( ["dk-prod-es-01"]="us-east1-b" ["dk-prod-es-02"]="us-east1-c" ["dk-prod-es-03"]="us-east1-d" )

### create 3 elasticsearch systems system
for GCP_INSTANCE_NAME in dk-prod-es-0{1..3}; do
  # Create 3 instances without external IP
  gcloud compute --project="${GCP_PROJECT}" instances create "${GCP_INSTANCE_NAME}" \
    --zone="${ZONES[$GCP_INSTANCE_NAME]}" \
    --machine-type='n1-standard-1' \
    --subnet='default' \
    --maintenance-policy='MIGRATE' \
    --service-account="${GCP_SERVICE_ACCT}" \
    --scopes="${SCOPE_URLS}" \
    --min-cpu-platform='Automatic' \
    --tags=elasticsearch \
    --image='ubuntu-1404-trusty-v20180308' \
    --image-project='ubuntu-os-cloud' \
    --boot-disk-size='10GB' \
    --boot-disk-type='pd-standard' \
    --boot-disk-device-name="${GCP_INSTANCE_NAME}"

  # Enable Project-Wide SSH Key (if not enabled already)
  gcloud compute instances add-metadata "${GCP_INSTANCE_NAME}" \
    --metadata block-project-ssh-keys='FALSE'
done
