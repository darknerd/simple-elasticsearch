#!/usr/bin/env bash

############################################################
# Validate Requirements
############################################################
CONFIG_DATA=${CONFIG_DATA:-"gce.json"}
[[ -z "${GCP_SERVICE_ACCT}" ]] && { echo 'Error: GCP_SERVICE_ACCT not set. Exiting' 1>&2; exit 1; }
[[ -z "${GCP_PROJECT}" ]] && { echo 'Error: GCP_PROJECT not set. Exiting' 1>&2; exit 1; }
[[ -f "${CONFIG_DATA}" ]] || { echo 'Error: Cannot find your cloud menu' 1>&2; exit 1; }

############################################################
# Global DataStructures
############################################################
declare -A CURRENT_CONFIG
declare -A INSTANCES
SYSTEMS+=($(jq -r 'keys | .[]' ${CONFIG_DATA}))
KEYS+=($(jq -r ".[\"${SYSTEMS[0]}\"] | keys | .[]" ${CONFIG_DATA}))

############################################################
# Create Data Structure from JSON (GCE version)
############################################################
for SYSTEM in ${SYSTEMS[*]}; do
  # Extract Data from JSON
  for KEY in ${KEYS[*]}; do
    if [[ ${KEY} =~ scopes ]]; then
      SCOPES=()
      SCOPES+=($(jq -r ".[\"${SYSTEM}\"].\"${KEY}\" | .[]" ${CONFIG_DATA}))
      CURRENT_CONFIG[${KEY}]="${SCOPES[*]}"
    elif [[ ${KEY} =~ tags ]]; then
      TAGS=()
      TAGS+=($(jq -r ".[\"${SYSTEM}\"].\"${KEY}\" | .[]" ${CONFIG_DATA}))
      CURRENT_CONFIG[${KEY}]="${TAGS[*]}"
    elif [[ ${KEY} =~ metadata ]]; then
      METADATA_KEYS=()
      METADATA=()
      METADATA_KEYS+=($(jq -r ".[\"${SYSTEM}\"].\"${KEY}\" | keys | .[]" ${CONFIG_DATA}))
      for METADATA_KEY in ${METADATA_KEYS}; do
        METADATA_VAL=$(jq -r ".[\"${SYSTEM}\"].\"${KEY}\".\"${METADATA_KEY}\"" ${CONFIG_DATA})
        METADATA_STR="${METADATA_KEY}=${METADATA_VAL}"
        METADATA+=("${METADATA_STR}")
      done
      CURRENT_CONFIG[${KEY}]="${METADATA[*]}"
    else
      CURRENT_CONFIG[${KEY}]="$(jq -r ".[\"${SYSTEM}\"].\"${KEY}\"" ${CONFIG_DATA})"
    fi
  done

  # Build Multi-Dimensional Datastructure
  for CONFIG in "${!CURRENT_CONFIG[@]}"; do
    INSTANCES[$SYSTEM,$CONFIG]=${CURRENT_CONFIG[$CONFIG]}
  done

  # Cleanup
  CURRENT_CONFIG=()
done

############################################################
# Create Systems using data structure
############################################################
for SYSTEM in ${SYSTEMS[*]}; do
  # Split Space-Delimited Data
  SCOPES=($(echo ${INSTANCES[$SYSTEM,scopes]}))
  TAGS=($(echo ${INSTANCES[$SYSTEM,tags]}))
  TAGS_OPT=$([[ -z "${TAGS[*]}"  ]] || echo "--tags=$(IFS=,; echo ${TAGS[*]})")

  gcloud compute --project="${GCP_PROJECT}" instances create "${SYSTEM}" \
    --zone=${INSTANCES[$SYSTEM,zone]} \
    --machine-type=${INSTANCES[$SYSTEM,machine-type]} \
    --subnet='default' \
    --maintenance-policy='MIGRATE' \
    --service-account="${GCP_SERVICE_ACCT}" \
    --scopes=$(IFS=,; echo "${SCOPES[*]}") \
    --min-cpu-platform='Automatic' \
    ${TAGS_OPT} \
    --image=${INSTANCES[$SYSTEM,image]} \
    --image-project=${INSTANCES[$SYSTEM,image-project]} \
    --boot-disk-size=${INSTANCES[$SYSTEM,boot-disk-size]} \
    --boot-disk-type=${INSTANCES[$SYSTEM,boot-disk-type]} \
    --boot-disk-device-name="${SYSTEM}"

  # Enable Project-Wide SSH Key (if not enabled already)
  for METADATA_STR in ${INSTANCES[$SYSTEM,metadata]}; do
    STATUS=$(gcloud compute instances list \
              --filter="name=${SYSTEM} AND zone ~ ${INSTANCES[$SYSTEM,zone]}" \
              --format='table(name,status)' \
              | grep -v NAME \
              | awk '{print $2}'
            )
    if [[ ${STATUS} =~ "RUNNING" ]]; then
      gcloud compute instances add-metadata "${SYSTEM}" \
        --zone=${INSTANCES[$SYSTEM,zone]}
        --metadata ${METADATA_STR}
    fi
  done
done
