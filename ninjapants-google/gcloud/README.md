# **Google Cloud SDK**

GCloud SDK should be installed and authorized, and you should configured a project (or use the default)

## **GCloud SDK Configuration**

Verify you your current target project, and set this equal to `GCP_PROJECT` and `GCP_SERVICE_ACCT`.

```bash
# use initial service account
export GCP_SERVICE_ACCT=$(gcloud iam service-accounts list \
  --filter='email ~ [0-9]*-compute@.*' \
  --format='table(email)' \
  | grep -v EMAIL
)

# set to current configured project
export GCP_PROJECT=$(gcloud config list \
  --format 'value(core.project)'
)
```
## **Creating Instances Statically**

The example scripts are custom tailored to create four instances (3 x ES, 1 x Tools):

```bash
./gce-instance-tools.sh
./gce-instance-elasticsearch.sh
```

## **Creating Instances Using JSON**

Edit a `gce.json` to define your instances, or the given `gce_elasticsearch.json`.  After, run one of the example scripts below:

### **Shell**

```bash
CONFIG_DATA='gce_elasticsearch.json' ./create-instances.sh
```

### **Perl**

```bash
cpanm "JSON::MaybeXS"
cpanm --local-lib=~/perl5 local::lib && eval $(perl -I ~/perl5/lib/perl5/ -Mlocal::lib)
CONFIG_DATA='gce_elasticsearch.json' ./create-instances.pl
```

### **Ruby**

```bash
CONFIG_DATA='gce_elasticsearch.json' ./create-instances.rb
```

### **Python**

```bash
CONFIG_DATA='gce.json' ./create-instances.py2
# or
CONFIG_DATA='gce.json' ./create-instances.py3
```

## **Configuring Running Systems**

### **Using Chef**

If you have your own Chef repository with a configured `.chef/` configuration directory that points to your Chef Server, you can start a knife bootstrap process.  The bootstrap process will:

1. register node objects on the Chef Server that represents your sytems
2. install Chef agent on the remote systems
3. download credentials authorizing remote systems to access Chef Server
4. do a chef run (converge) applies changes specified in the *runlist*

An example bootstrap process would look like this:

```bash
# Example only
knife bootstrap "ubuntu@$EXTERNAL_IP" \
 --node-name $GCE_INSTANCE_NAME \
 --environment $CHEF_ENV \
 --run-list $RUNLIST \
 --ssh-user 'ubuntu' \
 --sudo \
 --identity-file ~/.ssh/gce.key
```
