#!/usr/bin/env bash
[[ -z "${GCP_SERVICE_ACCT}" ]] && { echo 'Error: GCP_SERVICE_ACCT not set. Exiting' 1>&2; exit 1; }

SCOPES=(devstorage.read_only
       logging.write
       monitoring.write
       pubsub
       servicecontrol
       service.management.readonly
       trace.append
       userinfo.email
       compute.readonly)
PREFIX='https://www.googleapis.com/auth'
SCOPE_URLS=$(IFS=, ; echo "${SCOPES[*]/#/$PREFIX/}")

### create tools system
GCP_INSTANCE_NAME='dk-prod-tools'

gcloud compute --project="${GCP_PROJECT}" instances create "${GCP_INSTANCE_NAME}" \
  --zone='us-east1-b' \
  --machine-type='n1-standard-1' \
  --subnet='default' \
  --maintenance-policy='MIGRATE' \
  --service-account="${GCP_SERVICE_ACCT}" \
  --scopes="${SCOPE_URLS}" \
  --min-cpu-platform='Automatic' \
  --tags=elasticsearch \
  --image='ubuntu-1404-trusty-v20180308' \
  --image-project='ubuntu-os-cloud' \
  --boot-disk-size='10GB' \
  --boot-disk-type='pd-standard' \
  --boot-disk-device-name="${GCP_INSTANCE_NAME}"

# Enable Project-Wide SSH Key (if not enabled already)
gcloud compute instances add-metadata "${GCP_INSTANCE_NAME}" \
  --metadata block-project-ssh-keys='FALSE'
