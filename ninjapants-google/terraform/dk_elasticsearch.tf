#####################################################################
# Variables
#####################################################################
variable "default-scope" {
  default = [
               "storage-ro",
               "logging-write",
               "monitoring-write",
               "pubsub",
               "service-management",
               "service-control",
               "trace-append"
            ]
  description = "default scope created when creating new GCP instance in dashboard"
}

variable "standard-scope" {
  default = ["compute-ro"]
  description = "standard scope for all systems"
}

variable "elasticsearch-scope" {
  default = ["compute-rw"]
  description = "scope required by elasticsearch with gce plugin"
}

#####################################################################
# VM Instances
#####################################################################
resource "google_compute_instance" "dk-prod-tools" {
    name = "dk-prod-tools"
    machine_type = "n1-standard-1"
    zone = "us-east1-b"
    allow_stopping_for_update = true

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.standard-scope)}"
    }
}

resource "google_compute_instance" "dk-es-01" {
    name = "dk-es-01"
    machine_type = "n1-standard-1"
    zone = "us-east1-b"
    allow_stopping_for_update = true

    tags = ["elasticsearch"]

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.elasticsearch-scope)}"
    }
}

resource "google_compute_instance" "dk-es-02" {
    name = "dk-es-02"
    machine_type = "n1-standard-1"
    zone = "us-east1-c"
    allow_stopping_for_update = true

    tags = ["elasticsearch"]

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.elasticsearch-scope)}"
    }
}

resource "google_compute_instance" "dk-es-03" {
    name = "dk-es-03"
    machine_type = "n1-standard-1"
    zone = "us-east1-d"
    allow_stopping_for_update = true

    tags = ["elasticsearch"]

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.elasticsearch-scope)}"
    }
}
