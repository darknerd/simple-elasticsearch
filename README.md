# **Simple ElasticSearch**

Welcome to Simple ElasticSearch.  This configures a small 3-node Elastic Search cluster using Chef 13.  

Currently the following environments have been tested:

* **development environment (local)**
   * **bootstrap**: knife-zero
   * **vm provision**: vagrant
* **Google Cloud**
   * **bootstrap**: knife (via ssh)
   * **vm provision**: bash (gcloud) or terraform
* **Amazon Web Servers**
   * **bootstrap**: knife (via ssh)
   * **vm provision**: bash (aws-cli), terraform, or ansible

Some notes on supported host systems:

* **Ubuntu 16.04 Xenial** (my development environment):
   *  tested and works fine using Virtualbox (KVM or Xen cannot be enabled on same host)
* **Mac OS X**:
    * should work if bash v4 is installed, see [Essential GNU Tools](https://github.com/darkn3rd/devops_box_guides/blob/master/mac.md#essential-gnu-tools)
* **Windows**
    * will NOT work (many tools only work w/ DOS Paths despite [MSYS2](https://www.msys2.org/) availability)

## **Implementation Notes**

This is segregated into two layers:
  1. cloud provisioning automation
  2. change configuration (desired state) automation

The segregation is done for two reasons, security and maintainability.  The security happens in locking down the infrastructure, to avoid the potential mistake where infrastructure is destroyed by accident. In a real production environment, only a few would have access global administrative rights, and others rights will be handed out based on role, such as creating buckets, changing DNS, or a configuring load balancer.  

For maintainability, allows the layers to be swapped in an out, as interchangeable components.  Integrating the layers together with a web of interdependencies locks the solution to one path, and makes it harder to maintain, less consistent.

## **Security Notes**

This project is a proof of concept, and implements basic pragmatic security.  Only SSH is open to the world, and other ports are open to those systems within the same VPC.  This may be adequate for a *staging environment*, with exception of credentials management, where access would be restricted using *least privilege* principals.  In real *staging environment* or even remote *development environment* the following should be configured with SSH:

1. No password with SSH allowed (especially if open to the world), otherwise, strict password policy would have to be enforced.
1. Firewall rule should restrict inbound to organized approved networks IP addresses (whitelisted)
1. Admin VPN to organization approved network (users/passwords administered through SSO, e.g. RADIUS and LDAP + Kerberos)

For a *production environment* the environment would locked down, where a bastion jump server is used for access to the system.  Optimally, there would be a front facing VPC for web servers, and one or two backend VPCs (middle tier and data tier), each with a subnet per availability zone.  Similar restrictions in the above SSH added.

## **Development Environment**

### **Prerequites**

* ChefDK: https://downloads.chef.io/chefdk
  * Knife-Zero: `chef gem install knife-zero`
* Vagrant: https://www.vagrantup.com/
* Virtualbox: https://www.virtualbox.org/wiki/Downloads

The ChefDK comes with its own embedded ruby and gems, which in most cases, this does not cause a problem.  If you wish to integrate ChefDK into your existing ruby workflow, here is some documentation on that:

  * [Three Rubies and a ChefDK](https://medium.com/@Joachim8675309/three-rubies-and-a-chefdk-74dc8c9149a7) - ChefDK integration (RVM or RBenv)
  * [A Tale of Two Rubies, Part I: Managing Rubies with RVM](https://medium.com/@Joachim8675309/a-tale-of-two-rubies-part-i-34e5658c5bfc)
  * [A Tale of Two Rubies, Part II: Managing Rubies with Rbenv](https://medium.com/@Joachim8675309/a-tale-of-two-rubies-part-ii-5c3904dc4b3b)

### **Vagrant Instructions**

For local development, you would run this:

```bash
vagrant up # bring up systems
vagrant status # report their status
vagrant ssh ${SYSTEM_NAME} # ssh into guest system
```

### **SSH Configuration (Optional)**

Outside of vagrant system, using tools that require ssh is complex without use of a ssh-config file, `ssh -F dev.ssh-config <system_name>`.

Unfortunately, the `knife` tool used for Chef orchestration does not support use of ssh-config file, despite underlying ruby library supporting the feature:

* [Chef Issue 7053](https://github.com/chef/chef/issues/7053)).  

We can work around this limitation with the command below:

```bash
./config/ssh-config.sh
cat dev.ssh-config >> ~/.ssh/config
```

### **Bootstrap Dev Systems**

In Chef workflow, you are required to have a Chef Server with your system registered with it, in order to run your cookbook.  For a local development environment, the requirement of a Chef Server is overkill.  

As an alternative, we will use the *Chef Zero* facility to run a small in-memory ChefServer to mock out the whole Chef workflow.  This requires installing Knife-Zero plug-in (`chef gem install knife-zero`), so that we can bootstrap our local virtual guests.

If all the systems are up and running, we can use wrapper script to bootstrap them all:

```bash
chef gem install knife-zero  # install knife-zero if not done already
./config/bootstrap.pl
```

### **Bootstrapping Manually per System**

To bootstrap a single system, we would do the following:

```bash
chef gem install knife-zero  # install knife-zero if not done already

knife zero bootstrap vagrant@${IP_ADDRESS} \
  --node-name ${SYSTEM_NAME} \
  --environment dev \
  --run-list ${RUN_LIST} \ # e.g. 'role[elasticsearch]'
  --ssh-user vagrant \
  --sudo \
  --identity-file "vagrant/machines/${SYSTEM_NAME}/virtualbox/private_key "\
  --overwrite
```

If we changed your `${HOME}/.ssh/config` earlier, then the command would be:

```bash
knife zero bootstrap vagrant@${SYSTEM_NAME} \
  --node-name ${SYSTEM_NAME} \
  --environment dev \
  --run-list ${RUN_LIST} \ # e.g. 'role[elasticsearch]'
  --ssh-user vagrant \
  --sudo \
  --overwrite
```

### **Unit Tests**

You can run unit tests from cookbooks and see the code coverage reports with:

```bash
pushd ninjapants-chef-repo/cookbooks/dk_nginx
chef exec rspec
popd
pushd ninjapants-chef-repo/cookbooks/dk_oracle_java
chef exec rspec
popd
pushd ninjapants-chef-repo/cookbooks/dk_elasticsearch
chef exec rspec
```

### **Smoke (Integration) Tests**

```bash
pushd ninjapants-chef-repo/cookbooks/dk_elasticsearch
chef exec kitchen create    # create VMs
chef exec kitchen converge  # chef_run
chef exec kitchen verify    # run tests
# Cleanup
chef exec kitchen destroy
popd
```

### **Testing Solution in Dev**

Once the systems are bootstrap, you can query one of the nodes for the cluster stats:

```bash
curl '172.16.0.32:9200/_nodes/stats/process?pretty'
curl '172.16.0.32:9200/_cluster/state?pretty'
```

## **Production Environment**

These have been testing in both Google Cloud and Amazon Web Services.  The steps for production are:

1. Build infrastructure with target guest systems in either Google Cloud or Amazon Web Services, which will install a deploy key used for configuring the system systems.
2. Edit environment files, `ninjapants-chef-repo/environments/<enviroment_name>.json` and put configure the attributes with required private IP address.
3. Bootstrap recently configured systems using their public IP addresses on either Google Cloud or AWS:
4. Test solution.

### **Building The Cloud Infrastructure**

Documentation for each environment:

  * [Google Compute Engine](ninjapants-google/README.md) - creating systems using gcloud sdk or terraform
  * [Amazon Elastic Compute Cloud](ninjapants-aws/README.md) - creating systems using aws-cli, terraform, ansible

Besides building the exact list of nodes, this process will also install deploy SSH keys used for configuring the services on each of the systems we created.

### **Configure Chef Environment**

There is a sample environment file for each cloud solution: `dk_gcp` or `dk_aws`.  Configure these with the proper private IP addresses.

* [Chef-Repo Docs](ninjapants-chef-repo/README.md) - configuring, uploading components to Chef Server, bootstrapping EC2 nodes

This is a small snippet of what you need to configure:

```json
{
  "name": "environment_name",
  "description": "description goes here",
  "chef_type": "environment",
  "json_class": "Chef::Environment",
  "default_attributes": {
    "dk_oracle_java": {
      "local_repo": "192.68.8.5"
    },
    "dk_elasticsearch": {
      "jvm_heap_size": "512m",
      "unicast_hosts": ["192.68.8.7","192.68.8.6","192.68.8.8"]
    }
  },
  "override_attributes": {
  },
  "cookbook_versions": {
  }
}
```

If you used `./config/bootstrap.pl`, there will be some example environment JSON files.

### **Bootstrapping Prod Systems**

You can bootstrap a system with a wrapper script:

```bash
# Bootstrap nodes on Google Cloud
CHEF_ENV=dk_gcp ./config/bootstrap.pl
# Bootstrap nodes on AWS
CHEF_ENV=dk_aws ./config/bootstrap.pl
```

### **Bootstrapping Manually**

After editing your environment file, you can manually bootstrap a single system with this:

```bash
knife bootstrap ubuntu@${CONFIG_IP_ADDRESS} \
  --node-name ${SYSTEM_NAME}
  --environment ${CHEF_ENV} \
  --run-list ${RUN_LIST} \ # e.g. 'role[elasticsearch]'
  --ssh-user 'ubuntu' \
  --sudo
```

### **Testing the Solution Production**

Afterwards, you can check the health of the cluster.  As the ElasticSearch systems are not publicly accessible, you need to ssh into a system first and then do the health checks:

```bash
export CHEF_ENV=dk_gcp # or CHEF_ENV=dk_aws
ssh -F ${CHEF_ENV}.ssh-config dk-prod-es-01
curl $(hostname -i):9200/_nodes/stats/process?pretty
curl $(hostname -i):9200/_cluster/state?pretty
```

## **References**

This are sources of information I looked up in doing this project.

* Development (Vagrant)
  * https://www.vagrantup.com/docs/virtualbox/configuration.html
* Production/Staging (AWS, GCP)
  * Terraform
    * Google Cloud: https://www.terraform.io/docs/providers/google/index.html
  * Google Cloud:
      * Scopes: https://cloud.google.com/sdk/gcloud/reference/compute/instances/create\
      * Terraform: https://www.terraform.io/docs/providers/google/
  * AWS
      * Ansible: http://docs.ansible.com/ansible/latest/scenario_guides/guide_aws.html
      * Terraform: https://www.terraform.io/docs/providers/aws/
      * References:
          * https://docs.aws.amazon.com/cli/latest/reference/ec2/
      * User Guides:
          * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html
* Change Configuration (Chef)
    * Chef Docs, General
        * https://docs.chef.io/knife.html
        * https://docs.chef.io/environments.html
        * https://docs.chef.io/roles.html
    * Chef Supermarket
        * Hostsfile Cookbook (Seth Vargo): https://supermarket.chef.io/cookbooks/hostsfile/
    * Chef Plug-ins
        * Knife Zero: http://knife-zero.github.io/
        * Knife Zero Berkshelf Integration: https://knife-zero.github.io/tips/with_cookbook_manager/
    * Chef Testing
        * https://www.sethvargo.com/chef-recipe-code-coverage/
* Services
    * Elastic Search Documentation
        * Reference: https://www.elastic.co/guide/en/elasticsearch/reference/index.html
        * GCE Plugin:
            * https://www.elastic.co/guide/en/elasticsearch/plugins/current/discovery-gce.html
