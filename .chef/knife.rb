chef_repo = File.join(File.dirname(__FILE__), '../ninjapants-chef-repo')
site_cookbooks = "#{chef_repo}/cookbooks"
berks_cookbooks = "#{chef_repo}/berks-cookbooks"

chef_server_url "http://127.0.0.1:8889"
cookbook_path [site_cookbooks, berks_cookbooks]

# Knife Zero Configs
knife[:before_bootstrap] = "cd #{chef_repo} && ./global_berks_vendor"
knife[:before_converge] = "cd #{chef_repo} && ./global_berks_vendor"
