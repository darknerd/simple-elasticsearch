# **Config and Scripts Directory**

## **Configuration Files**

* `hosts` - hosts file describing ip addresses and hostnames (`dev` only)
* `runlist.tsv` - contains ordered list systems (grep pattern of systems) to be configured as well as corresponding runlists

## **Scripts**

* `ssh-config.sh` - convenience script that builds `ssh-config` from `hosts` for use with ssh configuration
* `bootstrap.sh` - will run `knife bootstrap` on remote cloud systems, or `knife zero bootstrap` on local vagrant systems.  For remote cloud systems, fetches information using `gcloud compute instances list` or `aws ec2 describe-instances` commands.
* `bootstrap.pl` - similar bootstrap script, but in Perl.
