#!/usr/bin/env bash

# Reference:
#  * https://puppet.com/docs/puppet/5.3/install_linux.html
#  * https://puppet.com/docs/puppet/5.3/puppet_platform.html

###############################################################
# main()
###############################################################
main() {
  install_puppet5
  link_puppet5_bin
}

###############################################################
# install_puppet5()
###############################################################
install_puppet5() {
  # Download Key + Deb Entry as deb package
  wget https://apt.puppetlabs.com/puppet5-release-$(lsb_release -c -s).deb
  sudo dpkg -i puppet5-release-$(lsb_release -c -s).deb
  # Update Package List
  sudo apt-get update
  # Install Package
  sudo apt-get install -y puppet-agent
}

###############################################################
# link_puppet5_bin()
###############################################################
link_puppet5_bin() {
  # Make Binaries Accessible
  for ITEM in /opt/puppetlabs/bin/*; do
    # Skip Bolt if Ubuntu 14.04 to mimick package behavior
    if [[ "$(lsb_release -c -s)" == "trusty" ]]; then
      [[ ${ITEM##*/} == bolt ]] && continue
    fi

    # Link to Source
    sudo ln -sf /opt/puppetlabs/puppet/bin/wrapper.sh /usr/local/bin/${ITEM##*/}
  done
}

main
