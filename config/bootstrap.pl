#!/usr/bin/env perl
use strict;

# variables
my $chef_env = $ENV{'CHEF_ENV'} || 'dev';
my ($hostsfile, $runlistfile, $fh) = qw(hosts runlist.tsv);
my (%config_ips, %priv_ips, %runlists, @systems);
chdir "config" if (-f "./config/hosts");

##########################################################
# Build Structures Based on Environment
##########################################################
if ($chef_env eq 'dev') {
  # open hosts files with configured systems
  open($fh, '<:encoding(UTF-8)', $hostsfile)
    or die "Could not open file '$hostsfile' $!";

  # build data structures
  %config_ips = %priv_ips = map {
    chomp; my ($k,$v) = (split /\s/)[1,0]; $k => $v
  } (<$fh>);

  close($fh);
}
elsif ($chef_env =~ /gcp/ ) {
  # query GCloud GCE systems (default project)
  my $data = `gcloud compute instances list`;

  # build data structures
  for (split /\n/, $data) {
    next if $_ !~ /RUNNING$/; # skip header and non-running systems
    $_ =~ s/\s+/ /g; # squeeze extra spaces
    my ($priv_ip, $config_ip, $system) = (split)[3,4,0];
    $config_ips{$system} = $config_ip;
    $priv_ips{$system} = $priv_ip;
  }
}
elsif ($chef_env =~ /aws/ ) {
  # query AWS ec2 systems (default profile region)
  my $filters = "Name=instance-state-name,Values=running";
  my $query = "Reservations[].Instances[].[Tags[?Key==`Name`].Value[],PrivateIpAddress,PublicIpAddress]";
  my $data = `aws ec2 describe-instances --filters '$filters' --query '$query' --output text`;

  # reformat output to space delimited fields
  $data =~ s/\n(\w.*)\n/ \1\n/gm;
  $data =~ y/\t/ /;

  # build data structures
  foreach (split /\n/, $data) {
    my ($priv_ip, $config_ip, $system) = split;
    $config_ips{$system} = $config_ip;
    $priv_ips{$system} = $priv_ip;
  }
} else {
  print STDERR "CHEF_ENV '$chef_env' is invalid. Exiting!\n";
  exit 1;
}

##########################################################
# Build Run-List
##########################################################

# Format: <PATTERN>\t'<RUNLIST)>'
open(my $fh, '<:encoding(UTF-8)', $runlistfile)
  or die "Could not open file '$runlistfile' $!";

while (<$fh>) {
  chomp;
  my ($pattern, $runlist) = split /\t/; # extract search pattern and runlist
  my @matched_systems = grep { $_ =~ $pattern } keys %config_ips; # find matches
  my %matched_runlists = map { $_ => $runlist } @matched_systems; # temp hash
  %runlists = (%runlists, %matched_runlists); # append matches to hash
  push @systems, @matched_systems;  # ordered list of systems
}

close($fh);

##########################################################
# Build Global Attributes List
##########################################################
my ($local_repo_ip, $unicast_hosts, $json_attributes, @es_system_ips);

# build global json attributres based on system matches
foreach (@systems) {
  push @es_system_ips, "\"$priv_ips{$_}\"" if ($_ =~ /es-/);
  $local_repo_ip = $priv_ips{$_} if ($_ =~ /tools/);
}

$unicast_hosts = join(',', @es_system_ips);
$json_attributes = "{ \"dk_oracle_java\": { \"local_repo\": \"$local_repo_ip\" }, \"dk_elasticsearch\": { \"unicast_hosts\": [$unicast_hosts] }}";

##########################################################
# Run Knife & Create SSH Config File
##########################################################
my ($runlist_opt, $keypath, $ssh_user, $zero_opt, $chef_config_opt,
    $json_attrib_opt, $knife_command, $ssh_config, $environment);

my $ssh_config_file = "../$chef_env.ssh-config";
my $environment_file = "../example.$chef_env.json";

unlink $ssh_config_file or warn "Could not delete $ssh_config_file: $!" if -e $ssh_config_file;
open(my $fh, ">>$ssh_config_file") or
  die "Could not open file '$ssh_config_file' $!";

foreach (@systems) {
  # get runlist option
  $runlist_opt = "--run-list $runlists{$_}" unless ($runlists{$_} eq "");

  # configure other command line options
  if ($chef_env eq 'dev') {
    $keypath = "../.vagrant/machines/$_/virtualbox/private_key";
    $ssh_user = 'vagrant';
    $zero_opt = 'zero';
    $chef_config_opt = '';
    $json_attrib_opt = '';
  } else {
    $keypath = "$ENV{'HOME'}/.ssh/@{[($chef_env =~ /gcp/) ? 'gce.key' : 'aws.pem' ]}";
    $ssh_user = 'ubuntu';
    $zero_opt = '';
    $chef_config_opt = "--config ../ninjapants-chef-repo/.chef/knife.rb";
    $json_attrib_opt = "--json-attributes '$json_attributes'";
  }

  # build command
  $knife_command = <<"EOT_KNIFE";
knife $zero_opt bootstrap $ssh_user\@$config_ips{$_} \\
  $chef_config_opt \\
  --node-name $_ \\
  --environment $chef_env \\
  $runlist_opt \\
  $json_attrib_opt \\
  --ssh-user $ssh_user \\
  --sudo \\
  --identity-file $keypath \\
  --yes
EOT_KNIFE

  $ssh_config = <<"EOT_SSH";
Host $_
  HostName $config_ips{$_}
  User $ssh_user
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile $keypath
  IdentitiesOnly yes
  LogLevel FATAL
EOT_SSH

  print "Knife Bootstrapping: $_ at $config_ips{$_}\n";
  $knife_command =~ s|  | |gms; # squeeze double-spaces
  $knife_command =~ s|\n\s*?\\\n|\n|gms; # filter out empty lines
  system("$knife_command\n");
  print $fh "$ssh_config\n";

}

close($fh);

##########################################################
# Create Example Chef environment.json file
##########################################################
open(my $fh, ">$environment_file") or
  die "Could not open file '$environment_file' $!";

$environment = <<"EOT_ENVIRON";
{
  "name": "$chef_env",
  "description": "DK ElasticSearch Environment",
  "chef_type": "environment",
  "json_class": "Chef::Environment",
  "default_attributes": {
    "dk_oracle_java": {
      "local_repo": "$local_repo_ip"
    },
    "dk_elasticsearch": {
      "jvm_heap_size": "512m",
      "unicast_hosts": [$unicast_hosts]
    }
  },
  "override_attributes": {
  },
  "cookbook_versions": {
      "dk_oracle_java": "= 0.1.0",
      "dk_nginx": "= 0.1.0"
  }
}
EOT_ENVIRON

print $fh "$environment\n";
