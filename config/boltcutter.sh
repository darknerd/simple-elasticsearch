#!/usr/bin/env bash
declare -A CONFIG_IPADDRS PRIVATE_IPADDRS RUNLISTS # hashes
declare -a SYSTEMS # arrays

PUPPET_ENV=${PUPPET_ENV:-'dev'}
[[ -f "./config/puppet_agent.sh" ]] && cd config

###############################################################
# main()
###############################################################
main() {
  echo "Using PUPPET_ENV=${PUPPET_ENV}..."
  SYSTEM_TABLE=$(fetch_system_list)
  # only continue if we have systems available
  if [[ ! -z ${SYSTEM_TABLE} ]]; then
    build_config_list "${SYSTEM_TABLE}"
    bolt_bootstrap_systems
  else
    echo 'No systems found. Exiting' 1>&2
    exit 1
  fi
}

###############################################################
# fetch_system_list()
#
# Purpose: Creates Space Seperated List:
#    <NAME> <PRIVATE_IPADDR> <CONFIG_IPADDR>
# NAME - describe name of system, name of node.
# PRIVATE_IPADDR - private internal IP address
# CONFIG_IPADDR - ip address used to configure system
###############################################################
fetch_system_list() {
  if [[ "${PUPPET_ENV}" == "dev" ]]; then
    echo "$(fetch_vagrant)"
  elif [[ "${PUPPET_ENV}" =~ "aws" ]]; then
    echo "$(fetch_ec2)"
  elif [[ "${PUPPET_ENV}" =~ "gcp" ]]; then
    echo "$(fetch_gce)"
  else
    echo "PUPPET_ENV '${PUPPET_ENV}' is invalid. Exiting" 1>&2
    exit 1
  fi
}

###############################################################
# fetch_vagrant() - create table from config hosts file used with Vagrant
###############################################################
fetch_vagrant() {
  HOSTS="$([[ -f hosts ]] && echo hosts || echo config/hosts)"
  while read I N; do printf "%s %s %s\n" $N $I $I;done <<< "$(cut -f1,2 $HOSTS)"
}

###############################################################
# fetch_gce() - create table from systems in Google Compute Engine
#
# Dependencies
# * Google Cloud SDK is configured with account that can access GCE instances
###############################################################
fetch_gce() {
  gcloud compute instances list \
    | awk '!/NAME/ { printf "%s %s %s\n", $1, $4, $5 }'
}

###############################################################
# fetch_ec2() - create table from systems in Amazon Elastic Compute Cloud
#
# Dependencies
# * AWS CLI is configured with account that can access EC2 instances
# * ${HOME}/.aws/config has default profile with region set
###############################################################
fetch_ec2() {
  # list ec2 instances for current region
  DATA=$(aws ec2 describe-instances \
    --filters 'Name=instance-state-name,Values=running' \
    --query 'Reservations[].Instances[].[Tags[?Key==`Name`].Value[],PrivateIpAddress,PublicIpAddress]' \
    --output text | sed '$!N;s/\n/ /' | tr '\t' ' ')
  while read -r I E N; do printf "%s %s %s\n" $N $I $E; done <<< "${DATA}"
}


###############################################################
# build_config_list( <space_delimited_list> )
#
# Purpose: Creates IP address assoc arrays, given space delimited list
#    <NAME> <PRIVATE_IPADDR> <CONFIG_IPADDR>
###############################################################
build_config_list() {
  while read NAME PRIV_IP CFG_IP; do
    CONFIG_IPADDRS["${NAME}"]="${CFG_IP}"   # used for bootstrap/provision
    PRIVATE_IPADDRS["${NAME}"]="${PRIV_IP}" # used in config of services
    SYSTEMS+=(${NAME})
  done <<< "${1}"
}

###############################################################
# bolt_bootstrap_systems( <space_delimited_list> )
#
# Purpose: Bootstraps nodes given PUPPET_ENV=<dev|dk_gcp|dk_aws>
# Dependencies:
#   * global SYSTEMS, CONFIG_IPADDRS
###############################################################
bolt_bootstrap_systems() {
  for SYSTEM in ${SYSTEMS[*]}; do
    if [[ "${PUPPET_ENV}" == "dev" ]]; then
      KEYPATH="../.vagrant/machines/${SYSTEM}/virtualbox/private_key"
      SSH_USER="vagrant"
    else
      KEYPATH="${HOME}/.ssh/$([[ ${PUPPET_ENV} =~ gcp ]] && echo gce.key || echo aws.pem)"
      SSH_USER="ubuntu"
    fi

    bolt script run puppet_agent.sh \
      --nodes ${CONFIG_IPADDRS[${SYSTEM}]} \
      --user ${SSH_USER} \
      --no-host-key-check \
      --private-key ${KEYPATH} \
  done
}

main
