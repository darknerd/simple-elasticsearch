#!/usr/bin/env bash
SPATH="$([[ -f hosts ]] && echo hosts || echo config/hosts)"
DPATH="$([[ -f hosts ]] && echo .. || echo .)/dev.ssh-config"
printf '' > ${DPATH}

while read -r -a LINE_ITEMS; do
  IPADDR=${LINE_ITEMS[0]}; HOSTNAME=${LINE_ITEMS[1]}
  cat <<-EOF >> ${DPATH}
Host ${HOSTNAME}
  HostName ${IPADDR}
  User vagrant
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile ${PWD}/.vagrant/machines/${HOSTNAME}/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
EOF
done < ${SPATH}
