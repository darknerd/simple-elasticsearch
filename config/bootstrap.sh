#!/usr/bin/env bash
declare -A CONFIG_IPADDRS PRIVATE_IPADDRS RUNLISTS # hashes
declare -a SYSTEMS # arrays

# Find Environment
[[ -f "./config/runlist.tsv" ]] && cd config
[[ -f runlist.tsv ]] || { echo "Error: 'runlist.tsv' not found. Exiting" 1>&2; exit 1; }

RUNLIST_FILE='runlist.tsv'
CHEF_ENV=${CHEF_ENV:-'dev'}
JSON_ATTRIB=""

###############################################################
# main()
###############################################################
main() {
  echo "Using CHEF_ENV=${CHEF_ENV}..."
  SYSTEM_TABLE=$(fetch_system_list)

  # only continue if we have systems available
  if [[ ! -z ${SYSTEM_TABLE} ]]; then
    build_config_list "${SYSTEM_TABLE}"
    build_run_list "${SYSTEM_TABLE}"
    build_attrib_list
    knife_bootstrap_systems
  else
    echo 'No systems found. Exiting' 1>&2
    exit 1
  fi
}

###############################################################
# fetch_system_list()
#
# Purpose: Creates Space Seperated List:
#    <NAME> <PRIVATE_IPADDR> <CONFIG_IPADDR>
# NAME - describe name of system, name of node.
# PRIVATE_IPADDR - private internal IP address
# CONFIG_IPADDR - ip address used to configure system
###############################################################
fetch_system_list() {
  if [[ "${CHEF_ENV}" == "dev" ]]; then
    echo "$(fetch_vagrant)"
  elif [[ "${CHEF_ENV}" =~ "aws" ]]; then
    echo "$(fetch_ec2)"
  elif [[ "${CHEF_ENV}" =~ "gcp" ]]; then
    echo "$(fetch_gce)"
  else
    echo "CHEF_ENV '${CHEF_ENV}' is invalid. Exiting" 1>&2
    exit 1
  fi
}

###############################################################
# fetch_vagrant() - create table from config hosts file used with Vagrant
###############################################################
fetch_vagrant() {
  HOSTS="$([[ -f hosts ]] && echo hosts || echo config/hosts)"
  while read I N; do printf "%s %s %s\n" $N $I $I;done <<< "$(cut -f1,2 $HOSTS)"
}

###############################################################
# fetch_gce() - create table from systems in Google Compute Engine
#
# Dependencies
# * Google Cloud SDK is configured with account that can access GCE instances
###############################################################
fetch_gce() {
  gcloud compute instances list \
    | awk '!/NAME/ { printf "%s %s %s\n", $1, $4, $5 }'
}

###############################################################
# fetch_ec2() - create table from systems in Amazon Elastic Compute Cloud
#
# Dependencies
# * AWS CLI is configured with account that can access EC2 instances
# * ${HOME}/.aws/config has default profile with region set
###############################################################
fetch_ec2() {
  # list ec2 instances for current region
  DATA=$(aws ec2 describe-instances \
    --filters 'Name=instance-state-name,Values=running' \
    --query 'Reservations[].Instances[].[Tags[?Key==`Name`].Value[],PrivateIpAddress,PublicIpAddress]' \
    --output text | sed '$!N;s/\n/ /' | tr '\t' ' ')
  while read -r I E N; do printf "%s %s %s\n" $N $I $E; done <<< "${DATA}"
}


###############################################################
# build_config_list( <space_delimited_list> )
#
# Purpose: Creates IP address assoc arrays, given space delimited list
#    <NAME> <PRIVATE_IPADDR> <CONFIG_IPADDR>
###############################################################
build_config_list() {
  while read NAME PRIV_IP CFG_IP; do
    CONFIG_IPADDRS["${NAME}"]="${CFG_IP}"   # used for bootstrap/provision
    PRIVATE_IPADDRS["${NAME}"]="${PRIV_IP}" # used in config of services
  done <<< "${1}"
}

###############################################################
# build_run_list( <space_delimited_list> )
#
# Purpose: Creates ordered list of system and runlist assoc array given
#  space delimited list in data file:
#    <NAME> <PRIVATE_IPADDR> <CONFIG_IPADDR>
# Dependencies:
#   * tab seperated file:
#      <GREP_PATTERN>\t<RUN_LIST>
###############################################################
build_run_list() {
  while read PATTERN RUNLIST; do
    # Grab Matches from Configuration File
    MATCHES=$(grep ${PATTERN} <<< "${1}")
    [[ -z "${MATCHES}" ]] && continue
    while IFS=$'\n' read MATCH; do
      SYSTEM=${MATCH%% *}
      SYSTEMS+=(${SYSTEM}) # build ordered list of systems
      RUNLISTS["${SYSTEM}"]="${RUNLIST}" # built runlist
    done <<< "${MATCHES}"
  done < ${RUNLIST_FILE}
}

###############################################################
# build_attrib_list( )
#
# This function is inherently tailored to the data, and must be
# change per configuration needs.
###############################################################
build_attrib_list() {
  declare -a ES_SYSTEM_IPS
  # Build Elastic Search JSON Attributes
  for SYSTEM in ${SYSTEMS[*]}; do
    if [[ $SYSTEM =~ "es-" ]]; then
      ES_SYSTEM_IPS+=("\"${PRIVATE_IPADDRS[$SYSTEM]}\"")
    elif [[ $SYSTEM =~ "tools" ]]; then
      LOCAL_REPO="${PRIVATE_IPADDRS[$SYSTEM]}"
    fi
  done
  UNICAST_HOSTS=$(echo ${ES_SYSTEM_IPS[*]} | tr ' ' ',')
  JSON_ATTRIB="{ \"dk_oracle_java\": { \"local_repo\": \"${LOCAL_REPO}\" }, \"dk_elasticsearch\": { \"unicast_hosts\": [${UNICAST_HOSTS}] }}"
}

###############################################################
# build_run_list( <space_delimited_list> )
#
# Purpose: Bootstraps nodes given CHEF_ENV=<dev|dk_gcp|dk_aws>
# Dependencies:
#   * global SYSTEMS, RUNLISTS, CONFIG_IPADDRS
###############################################################
knife_bootstrap_systems() {
  for SYSTEM in ${SYSTEMS[*]}; do
    # create runlist options if there are runlists
    RUNLIST="${RUNLISTS[${SYSTEM}]}"
    [[ "${RUNLIST}" == "" ]] || RUNLIST_OPT="--run-list ${RUNLIST}"

    if [[ "${CHEF_ENV}" == "dev" ]]; then
      KEYPATH="../.vagrant/machines/${SYSTEM}/virtualbox/private_key"
      SSH_USER="vagrant"
      ZERO_OPT="zero"
      CHEF_CONFIG=""
      JSON_ATTRIB_OPT="" # don't need override as static attributes
    else
      KEYPATH="${HOME}/.ssh/$([[ ${CHEF_ENV} =~ gcp ]] && echo gce.key || echo aws.pem)"
      SSH_USER="ubuntu"
      ZERO_OPT=""
      CHEF_CFG_OPT="--config ../ninjapants-chef-repo/.chef/knife.rb"
      JSON_ATTRIBUTES_OPT="--json-attributes '${JSON_ATTRIB}'"
    fi

    eval knife ${ZERO_OPT} bootstrap ${SSH_USER}@${CONFIG_IPADDRS[${SYSTEM}]} \
      ${CHEF_CFG_OPT} \
      --node-name ${SYSTEM} \
      --environment ${CHEF_ENV} \
      ${RUNLIST_OPT} \
      ${JSON_ATTRIBUTES_OPT} \
      --ssh-user ${SSH_USER} \
      --sudo \
      --identity-file ${KEYPATH} \
      --yes
  done
}

main
