#
# Cookbook:: dk_elasticsearch
# Recipe:: config
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

execute 'Enable limits.conf for processes started by init.d on Trusty' do
  command "sed -r -i 's/#?\s+(session\s+required\s+pam_limits.so)/\1/' /etc/pam.d/su"
end

execute 'Configure ulimit for elasticsearch user' do
  user, type, item, value = 'elasticsearch', '-', 'nofile', '65536'
  limits_entry = "%-15s %-5s %8s %14s" % [user, type, item, value]
  command "sed -i '/# End of file/ i\\#{limits_entry}' /etc/security/limits.conf"
end

template '/etc/elasticsearch/elasticsearch.yml' do
  source 'elasticsearch.yml.erb'
  variables(
    cluster_name: node['dk_elasticsearch']['cluster_name'],
    network_host: node['dk_elasticsearch']['network_host'],
    unicast_hosts: node['dk_elasticsearch']['unicast_hosts']
  )
  owner 'root'
  group 'elasticsearch'
  mode '0660'
  notifies :restart, 'service[elasticsearch]', :delayed
end

template '/etc/elasticsearch/jvm,options' do
  source 'jvm.options.erb'
  variables(
    jvm_init_heap: node['dk_elasticsearch']['jvm_heap_size'],
    jvm_max_heap: node['dk_elasticsearch']['jvm_heap_size']
  )
  owner 'root'
  group 'elasticsearch'
  mode '0660'
  notifies :restart, 'service[elasticsearch]', :delayed
end

template '/etc/elasticsearch/log4j2.properties' do
  source 'log4j2.properties.erb'
  owner 'root'
  group 'elasticsearch'
  mode '0660'
  notifies :restart, 'service[elasticsearch]', :delayed
end
