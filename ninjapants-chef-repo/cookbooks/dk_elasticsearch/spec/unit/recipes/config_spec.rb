#
# Cookbook:: dk_elasticsearch
# Spec:: config
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

require 'spec_helper'

describe 'dk_elasticsearch::config' do
  context 'Configure ElasticSearch on Ubuntu 14.04' do
    let(:server) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '14.04')
    end

    # inject service resource to test notifications in run context
    let(:chef_run) do
      server.converge(described_recipe) do
        server.resource_collection.insert(
          Chef::Resource::Service.new('elasticsearch', server.run_context)
        )
      end
    end

    let(:es_config_files) { %w(/etc/elasticsearch/log4j2.properties /etc/elasticsearch/jvm,options /etc/elasticsearch/elasticsearch.yml) }

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'enables limits.conf for init.d scripts' do
      expect(chef_run).to run_execute('Enable limits.conf for processes started by init.d on Trusty')
    end

    it 'configures ulimit for elasticsarch user' do
      expect(chef_run).to run_execute('Configure ulimit for elasticsearch user')
    end

    it 'creates elasticsearch configurations config from template' do
      es_config_files.each do |config_file|
        expect(chef_run).to create_template(config_file).with(
          owner: 'root',
          group: 'elasticsearch',
          mode: '0660'
        )
      end
    end
  end
end
