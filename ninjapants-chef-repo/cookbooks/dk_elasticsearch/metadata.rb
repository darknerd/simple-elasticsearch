name 'dk_elasticsearch'
maintainer 'Joaquín Menchaca'
maintainer_email 'suavecali@yahoo.com'
license 'MIT'
description 'Installs/Configures nginx'
long_description 'Installs/Configures nginx'
version '0.1.0'
chef_version '>= 12.1' if respond_to?(:chef_version)
supports 'ubuntu'
issues_url 'https://gitlab.com/darknerd/simple-elasticsearch/issues'
source_url 'https://gitlab.com/darknerd/simple-elasticsearch'

depends 'dk_oracle_java', '~> 0.1.2'
