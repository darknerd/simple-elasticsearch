name 'dk_oracle_java'
maintainer 'Joaquín Menchaca'
maintainer_email 'suavecali@yahoo.com'
license 'MIT'
description 'Installs/Configures dk_oracle_java'
long_description 'Installs/Configures dk_oracle_java'
version '0.1.2'
chef_version '>= 12.1' if respond_to?(:chef_version)
supports 'ubuntu'
issues_url 'https://gitlab.com/darknerd/simple-elasticsearch/issues'
source_url 'https://gitlab.com/darknerd/simple-elasticsearch'

depends 'hostsfile', '~> 3.0.1'
depends 'dk_nginx', '~> 0.1.0'
