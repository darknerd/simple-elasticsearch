# **dk_oracle_java**

This primary purpose of this cookbook will install Oracle Java 8 on a system.

There's a optional secondary feature (`repo.rb`) that downloads the Oracle tarball to a local web server to speed up installations.

## **Unit Tests**

```bash
chef exec rspec
```

## **Integration Tests**

```bash
chef exec kitchen converge
chef exec kitchen verify
```
