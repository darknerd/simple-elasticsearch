# # encoding: utf-8

# Inspec test for recipe dk_oracle_java::default

# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

describe apt('ppa:webupd8team/java') do
  it { should exist }
  it { should be_enabled }
end

describe packages(/oracle-java8-installer/) do
  its('statuses') { should cmp 'installed' }
  its('architectures') { should include 'all' }
end

describe file('/usr/lib/jvm/java-8-oracle/jre/bin/java') do
  it { should exist }
  it { should be_executable }
end

describe file('/usr/lib/jvm/java-8-oracle/bin/javac') do
  it { should exist }
  it { should be_executable }
end

describe file('/etc/profile.d/jdk.sh') do
  it { should exist }
  its('content') { should match /export JAVA_HOME=\/usr\/lib\/jvm\/java-8-oracle/ }
end

describe command('java') do
  it { should exist }
end

describe command('javac') do
  it { should exist }
end
