#
# Cookbook:: dk_oracle_java
# Recipe:: install
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

name = 'shared/accepted-oracle-license-v1-1'

# Prerequisites
package %w(python-software-properties debconf-utils)

# Add Apt Source
apt_repository 'webupd8team-java' do
  uri 'ppa:webupd8team/java'
end

# License Agreement
%w(select seen).each do |type|
  execute "Set Oracle License Agreement DebConf Question '#{type}'" do
    command "echo 'oracle-java8-installer #{name} #{type} true' | debconf-set-selections"
  end
end

# use local repo w/ java tarball if available
hostsfile_entry 'local_repo_entry' do
  hostname   'download.oracle.com'
  ip_address node['dk_oracle_java']['local_repo']
  action     :create
  not_if    { node['dk_oracle_java']['local_repo'].nil? } # explicit for ChefSpec
end

# Install Package after agreement
package 'oracle-java8-installer' do
  version "#{node['dk_oracle_java']['version']}-1~webupd8~0"
end
