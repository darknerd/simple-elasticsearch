#
# Cookbook:: dk_oracle_java
# Recipe:: repo
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# build Oracle Java URL
header = "Cookie: oraclelicense=accept-securebackup-cookie"
vers = node['dk_oracle_java']['version']
build = node['dk_oracle_java']['javas'][vers]['build']
hashcode = node['dk_oracle_java']['javas'][vers]['hashcode']
url = "http://download.oracle.com/otn-pub/java/jdk/#{vers}-#{build}/#{hashcode}/jdk-#{vers}-linux-x64.tar.gz"

# create docroot, assumes parent directories created
directory node['dk_oracle_java']['docroot'] do
  mode '0755'
  owner 'www-data'
  group 'www-data'
  action :create
  recursive true
end

# TODO: FC041 - Use https://docs.chef.io/resource_remote_file.html for http://www.foodcritic.io/#FC041
execute 'fetch oracle jdk' do
  cwd node['dk_oracle_java']['docroot'].to_s
  command "wget --no-cookies --no-check-certificate --header '#{header}' #{url}"
  not_if { File.exist?("#{node['dk_oracle_java']['docroot']}/jdk-#{vers}-linux-x64.tar.gz") }
end

execute "chown #{node['dk_oracle_java']['docroot']} to www-data" do
  command "chown -R www-data:www-data #{node['dk_oracle_java']['docroot']}"
end

template '/etc/nginx/sites-available/download.oracle.com' do
  source 'download.oracle.com.nginx.conf.erb'
  variables docroot: node['dk_oracle_java']['docroot']
end

link '/etc/nginx/sites-enabled/download.oracle.com' do
  to '/etc/nginx/sites-available/download.oracle.com'
  notifies :reload, 'service[nginx]', :delayed
end

service 'nginx' do
  supports(
    status: true,
    restart: true,
    reload: true
  )
end
