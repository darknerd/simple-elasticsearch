# **Environments**

You create your environment settings here per your environment.  After creating the necessary systems (in this case 3 elasticsearch nodes, 1 tools system), create an environment file, e.g. `mycompany.json`, and add the internal IP addresses needed:

```json
...
    "dk_oracle_java": {
      "local_repo": "10.142.0.2"
    },
    "dk_elasticsearch": {
      "jvm_heap_size": "2g",
      "unicast_hosts": ["10.142.0.3","10.142.0.4","10.142.0.5"]
    }
...
```

After creating the environment, you can upload it with this:

    knife environment from file environments/acme_staging.json

For more information on environments, see the Chef wiki page:

* https://docs.chef.io/environments.html
