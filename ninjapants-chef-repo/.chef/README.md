# Chef Server Knife Configuration

This is where you place your chef server access key and configuration in `knife.rb` configured to use Chef Server and corresponding authorized account.
